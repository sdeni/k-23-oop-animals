//
// Created by sd on 05/10/2020.
//

#ifndef K_23_LEC_OOP2_ANIMAL_H
#define K_23_LEC_OOP2_ANIMAL_H

class Animal {
private:
    int age;
    int *friends;
    const int MAX_FRIENDS = 1000;

protected:
    int weight;

public:

    Animal() {
        age = 1;
        weight = 1;
        friends = new int[100];
    }

    ~Animal() {
        delete[] friends;
    }

    void eat();
    int getWeight();

    int getAge();
    void setAge(int newAge);

    std::string name;
};

#endif //K_23_LEC_OOP2_ANIMAL_H
