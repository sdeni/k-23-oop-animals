//
// Created by sd on 05/10/2020.
//

#include <string>
#include <iostream>

#include "cat.h"

void Cat::voice() {
    std::cout << "Meow! My name is " << name << std::endl;
}
