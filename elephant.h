//
// Created by sd on 05/10/2020.
//

#ifndef K_23_LEC_OOP2_ELEPHANT_H
#define K_23_LEC_OOP2_ELEPHANT_H

#include "animal.h"

class Elephant : public Animal {
public:

    Elephant() {
        weight = 100;
    }

    void voice();
};

#endif //K_23_LEC_OOP2_ELEPHANT_H
